Name:           libexif
Summary:        Library for extracting extra information from image files
Version:        0.6.24
Release:        1
License:        LGPLv2+
URL:            https://libexif.github.io/

Source0:        https://github.com/libexif/libexif/archive/libexif-%(echo %{version} | sed "s/\./_/g")-release.tar.gz

BuildRequires:  autoconf automake doxygen gettext-devel libtool pkgconfig

%description
Most digital cameras produce EXIF files, which are JPEG files with
extra tags that contain information about the image. The EXIF library
allows you to parse an EXIF file and read the data from those tags.

%package        devel
Summary:        Files needed for libexif application development
Requires:       %{name}%{?_isa} = %{version}-%{release} pkgconfig
Requires:       pkgconfig

%description devel
The libexif-devel package contains the libraries and header files
for writing programs that use libexif.

%package_help

%prep
%autosetup -n libexif-libexif-0_6_24-release -p1 

%build
autoreconf -fiv
%configure --disable-static
%make_build

%install
%make_install
%delete_la

rm -rf %{buildroot}%{_datadir}/doc/libexif
cp -R doc/doxygen-output/libexif-api.html .
iconv -f latin1 -t utf-8 < COPYING > COPYING.utf8; cp COPYING.utf8 COPYING
iconv -f latin1 -t utf-8 < README > README.utf8; cp README.utf8 README
%find_lang libexif-12

%check
make check

%ldconfig_scriptlets

%files -f libexif-12.lang
%defattr(-,root,root)
%doc README
%license COPYING
%{_libdir}/libexif.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/libexif
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files help
%defattr(-,root,root)
%doc libexif-api.html NEWS

%changelog
* Fri Aug 18 2023 wangqia <wangqia@uniontech.com> - 0.6.24-1
- update to 0.6.24

* Tue Oct 18 2022 wangkerong <wangkerong@h-partners.com> - 0.6.21-3
- fix fuzz test error

* Thu Sep 10 2020 hanhui <hanhui15@huawei.com> - 0.6.21-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify source url

* Sat Aug 8 2020 yanan <yanan@huawei.com> - 0.6.22-1
- update to 0.6.22

* Tue Mar 10 2020 songnannan <songnannan2@huawei.com> - 0.6.21-20
- bugfix in oss-fuzz

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.6.21-19
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license file

* Thu Sep 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.6.21-18
- Package init
